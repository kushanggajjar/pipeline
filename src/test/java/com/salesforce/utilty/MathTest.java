package com.salesforce.utilty;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import com.salesforce.utility.Math;

public class MathTest
{
    @Test
    public void testAdd()
    {
        Math math = new Math();
        assertEquals(20, math.add(10, 10));
    }
}
